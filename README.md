JPS Regex Synchronizer
=======================================

Updates the regex of advanced searches and smart groups in a JPS instance based on a generated json file that contains all unique uses of regex in a JPS instance. The script will update an regex that is changed in the json file before rebuilding the json file with the current regex inventory.

## Reasoning for script
We use a lot of regex and need to have insight on what we already have in place and have the ability to update that regex in every place that it exists without having to just remember where it is among searches and groups.

## Usage
If you are using this for the first time you need to run it once to generate the jps_regex.json file. After this is generated edit it like below to update regex across different groups and advanced searches

```json
{
        "regex": "(Chicago)",
        "advanced_computer_searches": [
            {
                "id": 236,
                "name": "!Test - regex test",
                "criteria": {
                    "name": "Building",
                    "priority": 0,
                    "and_or": "and",
                    "search_type": "matches regex",
                    "value": "(Chicago)",
                    "opening_paren": false,
                    "closing_paren": false
                }
            }
        ],
        "advanced_mobile_device_searches": [],
        "advanced_user_searches": [],
        "mobile_device_groups": [],
        "computer_groups": [],
        "user_groups": []
    }
```

To change any search or group that uses the regex "(Chicago)" like above to "(Minneapolis|Chicago)" edit the "regex" value like below.

```json
{
        "regex": "(Minneapolis|Chicago)",
        "advanced_computer_searches": [
            {
                "id": 236,
                "name": "!Test - regex test",
                "criteria": {
                    "name": "Building",
                    "priority": 0,
                    "and_or": "and",
                    "search_type": "matches regex",
                    "value": "(Chicago)",
                    "opening_paren": false,
                    "closing_paren": false
                }
            }
        ],
        "advanced_mobile_device_searches": [],
        "advanced_user_searches": [],
        "mobile_device_groups": [],
        "computer_groups": [],
        "user_groups": []
    }
```
After this is changed run the script again to update these changes to the JPS.

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace infront of them. This will make it so your JPS_PASSWORD isn't in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamf.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
export MULTITHREAD_WORKERS=5
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_regex_synchronizer.py
```

### GitLab
Update the jps_regex.json file like above and then commit the change. A pipeline should automatically run to update the changes you made to the JPS. 

## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
    * Advanced Computer Searches
        * Read
        * Update
    * Advanced Mobile Device Searches
        * Read
        * Update
    * Advanced User Searches
        * Read
        * Update
    * Smart Computer Groups
        * Read
        * Update
    * Smart Mobile Device Groups
        * Read
        * Update
    * Smart User Groups
        * Read
        * Update

### GITLAB_TOKEN
Access token that can read and write to the project repository in GitLab.

### MULTITHREAD_WORKERS
This is a global variable and does not need to be set in this project.

Number of multithread workers to run Jamf API requests, a higher number will complete the script faster. I would recommend setting it to no higher than 5 because otherwise the JPS will start returning 502 errors on some requests.

## Authors
Bryan Weber (bweber26@cvtc.edu)

## Copyright
JPS Regex Synchronizer is Copyright 2022 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.