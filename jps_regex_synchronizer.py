from os import environ
import json
import xmltodict
from concurrent.futures import ThreadPoolExecutor
from jps_api_wrapper.classic import Classic
from jps_api_wrapper.pro import Pro


def add_items_to_unique_regex(jps_list: list, unique_regex: list, get_type: str):
    """
    Takes the result of a get group or advanced search api request and adds
    any unique regex criteria to an existing list of regex dictionaries

    :param jps_list:
        List of items from a get api request from advanced searches or
        groups

    :param unique_regex:
        List of dictionaries that contain unique regex, it is built out from
        consecutive runs so it can be empty or partially filled

    :param get_type:
        The endpoint that the jps_list items from from
        e.g. advanced_computer_searches

    :returns: Updated unique_regex list
    """
    for jps_item in jps_list:
        for criteria in jps_item["criteria"]:
            if "regex" in criteria["search_type"]:
                if not any(d.get("regex") == criteria["value"] for d in unique_regex):
                    unique_regex.append({"regex": criteria["value"]})
                for regex_dict in unique_regex:
                    if criteria["value"] in regex_dict.values():
                        regex_dict.setdefault(get_type, [])
                        regex_dict[get_type].append(
                            {
                                "id": jps_item["id"],
                                "name": jps_item["name"],
                                "criteria": criteria,
                            }
                        )

    return unique_regex


def sync_new_regex(
    regex_dict: dict,
    jps_item: dict,
    jps_endpoint_type: str,
    get_xml_method: Classic,
    update_xml_method: Classic,
):
    """
    Checks to see if regex in the top level of the dictionary
    matches what is set in the criteria of the searches and
    groups it is present in. If they are different it will
    update the JPS search or group to the new regex.

    :param regex_dict:
        Dictionary containing regex and all searches and groups
        that it is in

    :param jps_item:
        Individual search or group item that contains id, name,
        and criteria the regex is included in

    :param jps_endpoint_type:
        What endpoint the type of device is for searching
        through the dictionary

    :param get_xml_method:
        The OrganicJamf method that will be used to get the
        xml data from
        e.g. OrganicJamf.get_user_group_xml()

    :param update_xml_method:
        The OrganicJamf method that will be used to update with
        the modified xml
        e.g. OrganicJamf.update_user_group()
    """
    if regex_dict["regex"] != jps_item["criteria"]["value"]:
        jps_xml = xmltodict.parse(get_xml_method(jps_item["id"]))
        current_criteria = jps_xml[jps_endpoint_type]["criteria"]["criterion"]
        if type(current_criteria) != list:
            jps_xml[jps_endpoint_type]["criteria"]["criterion"]["value"] = regex_dict[
                "regex"
            ]
            r = update_xml_method(
                jps_item["id"],
                xmltodict.unparse(jps_xml),
            )
            print("UPDATED: Advanced Mobile Device Search - " + jps_item["name"])
        else:
            for i, criterion in enumerate(current_criteria):
                if (
                    criterion["name"] == jps_item["criteria"]["name"]
                    and criterion["search_type"] == jps_item["criteria"]["search_type"]
                ):
                    jps_xml[jps_endpoint_type]["criteria"]["criterion"][i][
                        "value"
                    ] = regex_dict["regex"]
                    update_xml_method(
                        jps_item["id"],
                        xmltodict.unparse(jps_xml),
                    )
                    print(
                        "UPDATED: Advanced Mobile Device Search - " + jps_item["name"]
                    )


def main():
    JPS_URL = environ["JPS_URL"]
    JPS_USERNAME = environ["JPS_USERNAME"]
    JPS_PASSWORD = environ["JPS_PASSWORD"]
    MULTITHREAD_WORKERS = int(environ["MULTITHREAD_WORKERS"])

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic, Pro(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as pro:
        executor = ThreadPoolExecutor(max_workers=MULTITHREAD_WORKERS)
        try:
            """Get json regex inventory from file"""
            with open("jps_regex.json", "r") as f:
                unique_regex = json.load(f)
        except FileNotFoundError:
            unique_regex = []

        """Update any changed regex in JPS instance"""
        for regex_dict in unique_regex:
            """
            This one is hard coded due to it using JPS Pro and having
            different keys
            """
            for mobile_search in regex_dict["advanced_mobile_device_searches"]:
                """If base regex does not match a criteria regex"""
                if regex_dict["regex"] != mobile_search["criteria"]["value"]:
                    mobile_search_xml = xmltodict.parse(
                        classic.get_advanced_mobile_device_search(mobile_search["id"], data_type="xml")
                    )
                    current_criteria = mobile_search_xml[
                        "advanced_mobile_device_search"
                    ]["criteria"]["criterion"]
                    for i, criterion in enumerate(current_criteria):
                        if (
                            criterion["name"] == mobile_search["criteria"]["name"]
                            and criterion["search_type"]
                            == mobile_search["criteria"]["searchType"]
                        ):
                            mobile_search_xml["advanced_mobile_device_search"][
                                "criteria"
                            ]["criterion"][i]["value"] = regex_dict["regex"]
                            classic.update_advanced_mobile_device_search(
                                xmltodict.unparse(mobile_search_xml),
                                mobile_search["id"],
                            )
                            print(
                                "UPDATED: Advanced Mobile Device Search - "
                                + mobile_search["name"]
                            )
            for computer_search in regex_dict["advanced_computer_searches"]:
                sync_new_regex(
                    regex_dict,
                    computer_search,
                    "advanced_computer_search",
                    classic.get_advanced_computer_search,
                    classic.update_advanced_computer_search,
                )
            for user_search in regex_dict["advanced_user_searches"]:
                sync_new_regex(
                    regex_dict,
                    user_search,
                    "advanced_user_search",
                    classic.get_advanced_user_search,
                    classic.update_advanced_user_search,
                )
            for mobile_device_group in regex_dict["mobile_device_groups"]:
                sync_new_regex(
                    regex_dict,
                    mobile_device_group,
                    "mobile_device_group",
                    classic.get_mobile_device_group,
                    classic.update_mobile_device_group,
                )
            for computer_group in regex_dict["computer_groups"]:
                sync_new_regex(
                    regex_dict,
                    computer_group,
                    "computer_group",
                    classic.get_computer_group,
                    classic.update_computer_group,
                )
            for user_group in regex_dict["user_groups"]:
                sync_new_regex(
                    regex_dict,
                    user_group,
                    "user_group",
                    classic.get_user_group,
                    classic.update_user_group,
                )

        unique_regex = []
        print("Rebuilding jps_regex.json...")

        """
        Add mobile device searches to master file
        This one is different than the others because it uses the Pro Api
        """
        mobile_device_searches = pro.get_advanced_mobile_device_searches()["results"]
        for search in mobile_device_searches:
            for criteria in search["criteria"]:
                if "regex" in criteria["searchType"]:
                    if not any(
                        d.get("regex") == criteria["value"] for d in unique_regex
                    ):
                        unique_regex.append({"regex": criteria["value"]})
                    for regex_dict in unique_regex:
                        if criteria["value"] in regex_dict.values():
                            regex_dict.setdefault("advanced_mobile_device_searches", [])
                            regex_dict["advanced_mobile_device_searches"].append(
                                {
                                    "id": search["id"],
                                    "name": search["name"],
                                    "criteria": criteria,
                                }
                            )

        """Add advanced user searches to unique_regex"""
        user_searches = classic.get_advanced_user_searches()["advanced_user_searches"]
        user_search_ids = [user_search["id"] for user_search in user_searches]
        user_searches = [
            user_search["advanced_user_search"]
            for user_search in executor.map(
                classic.get_advanced_user_search, user_search_ids
            )
        ]
        unique_regex = add_items_to_unique_regex(
            user_searches, unique_regex, "advanced_user_searches"
        )

        """Add advanced computer searches to unqiue_regex"""
        computer_searches = classic.get_advanced_computer_searches()[
            "advanced_computer_searches"
        ]
        computer_search_ids = [
            computer_search["id"] for computer_search in computer_searches
        ]
        computer_searches = [
            computer_search["advanced_computer_search"]
            for computer_search in executor.map(
                classic.get_advanced_computer_search, computer_search_ids
            )
        ]
        unique_regex = add_items_to_unique_regex(
            computer_searches, unique_regex, "advanced_computer_searches"
        )

        """Add mobile device groups to unique regex"""
        mobile_device_groups = classic.get_mobile_device_groups()["mobile_device_groups"]
        mobile_device_group_ids = [
            mobile_device_group["id"] for mobile_device_group in mobile_device_groups
        ]
        mobile_device_groups = [
            mobile_device_group["mobile_device_group"]
            for mobile_device_group in executor.map(
                classic.get_mobile_device_group, mobile_device_group_ids
            )
        ]
        unique_regex = add_items_to_unique_regex(
            mobile_device_groups, unique_regex, "mobile_device_groups"
        )

        """Add computer groups to unique regex"""
        computer_groups = classic.get_computer_groups()["computer_groups"]
        computer_group_ids = [
            computer_group["id"] for computer_group in computer_groups
        ]
        computer_groups = [
            computer_group["computer_group"]
            for computer_group in executor.map(
                classic.get_computer_group, computer_group_ids
            )
        ]
        unique_regex = add_items_to_unique_regex(
            computer_groups, unique_regex, "computer_groups"
        )

        """Add user groups to unique regex"""
        user_groups = classic.get_user_groups()["user_groups"]
        user_group_ids = [user_group["id"] for user_group in user_groups]
        user_groups = [
            user_group["user_group"]
            for user_group in executor.map(classic.get_user_group, user_group_ids)
        ]
        unique_regex = add_items_to_unique_regex(
            user_groups, unique_regex, "user_groups"
        )

        for regex_dict in unique_regex:
            regex_dict.setdefault("advanced_mobile_device_searches", [])
            regex_dict.setdefault("advanced_computer_searches", [])
            regex_dict.setdefault("advanced_user_searches", [])
            regex_dict.setdefault("mobile_device_groups", [])
            regex_dict.setdefault("computer_groups", [])
            regex_dict.setdefault("user_groups", [])

        print("Done.")

        """Write regex inventory to file"""
        with open("jps_regex.json", "w") as f:
            json.dump(unique_regex, f, indent=4)


if __name__ == "__main__":
    main()
